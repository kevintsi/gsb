-- MySQL dump 10.16  Distrib 10.1.37-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: gsb_frais
-- ------------------------------------------------------
-- Server version	10.1.37-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `etat`
--

DROP TABLE IF EXISTS `etat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etat` (
  `id` char(2) NOT NULL,
  `libelle` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etat`
--

LOCK TABLES `etat` WRITE;
/*!40000 ALTER TABLE `etat` DISABLE KEYS */;
INSERT INTO `etat`
VALUES ('CL','Saisie clôturée'),
       ('CR','Fiche créée, saisie en cours'),
       ('RB','Remboursée'),
       ('VA','Validée et mise en paiement');
/*!40000 ALTER TABLE `etat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fichefrais`
--

DROP TABLE IF EXISTS `fichefrais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fichefrais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idVisiteur` char(4) NOT NULL,
  `mois` char(6) NOT NULL,
  `nbJustificatifs` int(11) DEFAULT NULL,
  `montantValide` decimal(10,2) DEFAULT NULL,
  `dateModif` date DEFAULT NULL,
  `idEtat` char(2) DEFAULT 'CR',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fichefrais_uk1` (`idVisiteur`,`mois`),
  KEY `fichefrais_ibfk_1` (`idEtat`),
  CONSTRAINT `fichefrais_ibfk_1` FOREIGN KEY (`idEtat`) REFERENCES `etat` (`id`),
  CONSTRAINT `fichefrais_ibfk_2` FOREIGN KEY (`idVisiteur`) REFERENCES `visiteur` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fichefrais`
--

LOCK TABLES `fichefrais` WRITE;
/*!40000 ALTER TABLE `fichefrais` DISABLE KEYS */;
INSERT INTO `fichefrais`
VALUES (4,'a131','201902',NULL,NULL,'2019-02-21','CR'),
       (5,'a17','201902',NULL,NULL,'2019-02-06','CR'),
       (6,'a131','201903',NULL,NULL,'2019-03-21','CR'),
       (7,'a131','201904',NULL,NULL,'2019-04-21','CR'),
       (8,'a131','201905',NULL,NULL,'2019-05-17','CR'),
       (9,'a17','201903',NULL,NULL,'2019-03-21','CR'),
       (10,'a17','201904',NULL,NULL,'2019-04-21','CR'),
       (11,'a17','201905',NULL,NULL,'2019-05-17','CR');
/*!40000 ALTER TABLE `fichefrais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fraisforfait`
--

DROP TABLE IF EXISTS `fraisforfait`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fraisforfait` (
  `id` char(3) NOT NULL,
  `libelle` char(20) DEFAULT NULL,
  `montant` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fraisforfait`
--

LOCK TABLES `fraisforfait` WRITE;
/*!40000 ALTER TABLE `fraisforfait` DISABLE KEYS */;
INSERT INTO `fraisforfait`
VALUES ('ETP','Forfait Etape',110.00),
       ('KM','Frais Kilométrique',0.62),
       ('NUI','Nuitée Hôtel',80.00),
       ('REP','Repas Restaurant',25.00);
/*!40000 ALTER TABLE `fraisforfait` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lignefraisforfait`
--

DROP TABLE IF EXISTS `lignefraisforfait`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lignefraisforfait` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idFicheFrais` int(11) NOT NULL,
  `idFraisForfait` char(3) NOT NULL,
  `quantite` int(11) DEFAULT NULL,
  `dateModification` date DEFAULT NULL,
  `idEtat` char(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lignefraisforfait_ibfk_1` (`idFicheFrais`),
  KEY `lignefraisforfait_ibfk_2` (`idFraisForfait`),
  KEY `lignefraisforfait_ibfk_3` (`idEtat`),
  CONSTRAINT `lignefraisforfait_ibfk_1` FOREIGN KEY (`idFicheFrais`) REFERENCES `fichefrais` (`id`),
  CONSTRAINT `lignefraisforfait_ibfk_2` FOREIGN KEY (`idFraisForfait`) REFERENCES `fraisforfait` (`id`),
  CONSTRAINT `lignefraisforfait_ibfk_3` FOREIGN KEY (`idEtat`) REFERENCES `etat` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lignefraisforfait`
--

LOCK TABLES `lignefraisforfait` WRITE;
/*!40000 ALTER TABLE `lignefraisforfait` DISABLE KEYS */;
INSERT INTO `lignefraisforfait`
VALUES (13,4,'ETP',0,'2019-02-21','CR'),
       (14,4,'KM',6,'2019-02-21','CR'),
       (15,4,'NUI',0,'2019-02-21','CR'),
       (16,4,'REP',7,'2019-02-21','CR'),

       (17,5,'ETP',2,'2019-02-06','CR'),
       (18,5,'KM',20,'2019-02-06','CR'),
       (19,5,'NUI',3,'2019-02-06','CR'),
       (20,5,'REP',4,'2019-02-06','CR'),

       (21,6,'ETP',2,'2019-03-21','CR'),
       (22,6,'KM',20,'2019-03-21','CR'),
       (23,6,'NUI',3,'2019-03-21','CR'),
       (24,6,'REP',4,'2019-03-21','CR'),

       (25,7,'ETP',2,'2019-04-21','CR'),
       (26,7,'KM',20,'2019-04-21','CR'),
       (27,7,'NUI',3,'2019-04-21','CR'),
       (28,7,'REP',4,'2019-04-21','CR'),

       (29,8,'ETP',0,'2019-03-21','CR'),
       (30,8,'KM',6,'2019-03-21','CR'),
       (31,8,'NUI',0,'2019-03-21','CR'),
       (32,8,'REP',7,'2019-03-21','CR'),

       (33,9,'ETP',2,'2019-05-17','CR'),
       (34,9,'KM',20,'2019-05-17','CR'),
       (35,9,'NUI',3,'2019-05-17','CR'),
       (36,9,'REP',4,'2019-05-17','CR'),

       (37,10,'ETP',2,'2019-04-21','CR'),
       (38,10,'KM',20,'2019-04-21','CR'),
       (39,10,'NUI',3,'2019-04-21','CR'),
       (40,10,'REP',4,'2019-04-21','CR'),

       (41,11,'ETP',2,'2019-05-17','CR'),
       (42,11,'KM',20,'2019-05-17','CR'),
       (43,11,'NUI',3,'2019-05-17','CR'),
       (44,11,'REP',4,'2019-05-17','CR');
/*!40000 ALTER TABLE `lignefraisforfait` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lignefraishorsforfait`
--

DROP TABLE IF EXISTS `lignefraishorsforfait`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lignefraishorsforfait` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idFicheFrais` int(11) NOT NULL,
  `libelle` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `montant` decimal(10,2) DEFAULT NULL,
  `dateModif` date DEFAULT NULL,
  `idEtat` char(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lignefraishorsforfait_ibfk_1` (`idFicheFrais`),
  KEY `lignefraishorsforfait_ibfk_2` (`idEtat`),
  CONSTRAINT `lignefraishorsforfait_ibfk_1` FOREIGN KEY (`idFicheFrais`) REFERENCES `fichefrais` (`id`),
  CONSTRAINT `lignefraishorsforfait_ibfk_2` FOREIGN KEY (`idEtat`) REFERENCES `etat` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lignefraishorsforfait`
--

LOCK TABLES `lignefraishorsforfait` WRITE;
/*!40000 ALTER TABLE `lignefraishorsforfait` DISABLE KEYS */;
INSERT INTO `lignefraishorsforfait`
VALUES (1,5,'Repas M ROSS',NULL,75.00,'2019-02-06','CR'),
       (2,5,'Utilisation d un VTC',NULL,25.00,'2019-02-06','CR'),
       (3,4,'Repas avec M PHAM',NULL,120.00,'2019-02-21','CR'),
       (4,4,'Utilisation d un VTC',NULL,25.00,'2019-02-21','CR'),
       (5,4,'Seul Hôtel disponible',NULL,120.00,'2019-02-21','CR'),
       (6,6,'Utilisation d un VTC',NULL,30.00,'2019-03-21','CR'),
       (7,6,'Repas avec Mme BETIER',NULL,120.00,'2019-03-21','CR'),
       (8,7,'Célébration signature du premier contrat avec M GILLET',NULL,50.00,'2019-04-21','CR'),
       (9,7,'Repas avec Mme DUBOCQ',NULL,120.00,'2019-04-21','CR'),
       (10,10,'Achat d\'un cadeau pour le nouveau né de M ESCOBAR' ,NULL,25.00,'2019-04-21','CR'),
       (11,11,'Seul Hôtel disponible',NULL,90.00,'2019-05-17','CR'),
       (12,11,'Utilisation d un VTC',NULL,35.00,'2019-05-17','CR'),
       (13,6,'Repas M NADAL',NULL,55.00,'2019-03-21','CR'),
       (14,6,'Utilisation d un VTC',NULL,25.00,'2019-03-21','CR'),
       (15,7,'Seul Hôtel disponible',NULL,49.00,'2019-04-21','CR'),
       (16,7,'Repas avec Mme GALLET',NULL,100.00,'2019-04-21','CR'),
       (17,7,'Repas avec M RAMIREZ',NULL,120.00,'2019-04-21','CR'),
       (18,8,'Achat d\'un cadeau pour le nouveau né de M FEDERER',NULL,25.00,'2019-05-17','CR'),
       (19,8,'Seul Hôtel disponible',NULL,120.00,'2019-05-17','CR'),
       (20,10,'Utilisation d un VTC',NULL,25.00,'2019-04-21','CR'),
       (21,10,'Seul Hôtel disponible',NULL,120.00,'2019-04-21','CR'),
       (22,9,'Utilisation d un VTC car ma voiture est en répartion',NULL,25.00,'2019-03-21','CR'),
       (23,9,'Repas avec M YURI',NULL,41.00,'2019-03-21','CR');
/*!40000 ALTER TABLE `lignefraishorsforfait` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visiteur`
--

DROP TABLE IF EXISTS `visiteur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visiteur` (
  `id` char(4) NOT NULL,
  `nom` char(30) DEFAULT NULL,
  `prenom` char(30) DEFAULT NULL,
  `login` char(20) DEFAULT NULL,
  `mdp` char(255) DEFAULT NULL,
  `adresse` char(30) DEFAULT NULL,
  `cp` char(5) DEFAULT NULL,
  `ville` char(30) DEFAULT NULL,
  `dateEmbauche` date DEFAULT NULL,
  `estComptable` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visiteur`
--

LOCK TABLES `visiteur` WRITE;
/*!40000 ALTER TABLE `visiteur` DISABLE KEYS */;
INSERT INTO `visiteur`
VALUES ('a131','Villechalane','Louis','lvillachane','3abf9eb797afe468902101efe6b4b00f7d50802a','8 rue des Charmes','46000','Cahors','2005-12-21',0),
       ('a17','Andre','David','dandre','12e0b9be32932a8028b0ef0432a0a0a99421f745','1 rue Petit','46200','Lalbenque','1998-11-23',0),
       ('a55','Bedos','Christian','cbedos','a34b9dfadee33917a63c3cdebdc9526230611f0b','1 rue Peranud','46250','Montcuq','1995-01-12',0),
       ('a93','Tusseau','Louis','ltusseau','f1c1d39e9898f3202a2eaa3dc38ae61575cd77ad','22 rue des Ternes','46123','Gramat','2000-05-01',0),
       ('b13','Bentot','Pascal','pbentot','178e1efaf000fdf2267edc43fad2a65197a0ab10','11 allée des Cerises','46512','Bessines','1992-07-09',0),
       ('b16','Bioret','Luc','lbioret','ab7fa51f9bf8fde35d9e5bcc5066d3b71dda00d2','1 Avenue gambetta','46000','Cahors','1998-05-11',0),
       ('b19','Bunisset','Francis','fbunisset','aa710ca3a1f12234bc2872aa0a6f88d6cf896ae4','10 rue des Perles','93100','Montreuil','1987-10-21',0),
       ('b25','Bunisset','Denise','dbunisset','40ff56dc0525aa08de29eba96271997a91e7d405','23 rue Manin','75019','paris','2010-12-05',0),
       ('b28','Cacheux','Bernard','bcacheux','51a4fac4890def1ef8605f0b2e6554c86b2eb919','114 rue Blanche','75017','Paris','2009-11-12',0),
       ('b34','Cadic','Eric','ecadic','2ed5ee95d2588be3650a935ff7687dee46d70fc8','123 avenue de la République','75011','Paris','2008-09-23',0),
       ('b4','Charoze','Catherine','ccharoze','8b16cf71ab0842bd871bce99a1ba61dd7e9d4423','100 rue Petit','75019','Paris','2005-11-12',0),
       ('b50','Clepkens','Christophe','cclepkens','7ddda57eca7a823c85ac0441adf56928b47ece76','12 allée des Anges','93230','Romainville','2003-08-11',0),
       ('b59','Cottin','Vincenne','vcottin','2f95d1cac7b8e7459376bf36b93ae7333026282d','36 rue Des Roches','93100','Monteuil','2001-11-18',0),
       ('c14','Daburon','François','fdaburon','5c7cc4a7f0123460c29c84d8f8a73bc86184adbb','13 rue de Chanzy','94000','Créteil','2002-02-11',0),
       ('c3','De','Philippe','pde','03b03872dd570959311f4fb9be01788e4d1a2abf','13 rue Barthes','94000','Créteil','2010-12-14',0),
       ('c54','Debelle','Michel','mdebelle','1fa95c2fac5b14c6386b73cbe958b663fc66fdfa','181 avenue Barbusse','93210','Rosny','2006-11-23',0),
       ('d13','Debelle','Jeanne','jdebelle','18c2cad6adb7cee7884f70108cfd0a9b448be9be','134 allée des Joncs','44000','Nantes','2000-05-11',0),
       ('d51','Debroise','Michel','mdebroise','46b609fe3aaa708f5606469b5bc1c0fa85010d76','2 Bld Jourdain','44000','Nantes','2001-04-17',0),
       ('e22','Desmarquest','Nathalie','ndesmarquest','abc20ea01dabd079ddd63fd9006e7232e442973c','14 Place d Arc','45000','Orléans','2005-11-12',0),
       ('e24','Desnost','Pierre','pdesnost','8eaa8011ec8aa8baa63231a21d12f4138ccc1a3d','16 avenue des Cèdres','23200','Guéret','2001-02-05',0),
       ('e39','Dudouit','Frédéric','fdudouit','55072fa16c988da8f1fb31e40e4ac5f325ac145d','18 rue de l église','23120','GrandBourg','2000-08-01',0),
       ('e49','Duncombe','Claude','cduncombe','577576f0b2c56c43b596f701b782870c8742c592','19 rue de la tour','23100','La souteraine','1987-10-10',0),
       ('e5','Enault-Pascreau','Céline','cenault','cc0fb4115bb04c613fd1b95f4792fc44f07e9f4f','25 place de la gare','23200','Gueret','1995-09-01',0),
       ('e52','Eynde','Valérie','veynde','d06ace8d729693904c304625e6a6fab6ab9e9746','3 Grand Place','13015','Marseille','1999-11-01',0),
       ('f21','Finck','Jacques','jfinck','6d8b2060b60132d9bdb09d37913fbef637b295f2','10 avenue du Prado','13002','Marseille','2001-11-10',0),
       ('f39','Frémont','Fernande','ffremont','aa45efe9ecbf37db0089beeedea62ceb57db7f17','4 route de la mer','13012','Allauh','1998-10-01',0),
       ('f4','Gest','Alain','agest','1af7dedacbbe8ce324e316429a816daeff4c542f','30 avenue de la mer','13025','Berre','1985-11-01',0);
/*!40000 ALTER TABLE `visiteur` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-18 10:16:31

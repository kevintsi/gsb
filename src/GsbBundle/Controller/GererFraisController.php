<?php

namespace GsbBundle\Controller;

use DateTime;
use GsbBundle\Entity\Lignefraishorsforfait;
use GsbBundle\Form\LignefraishorsforfaitType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use GsbBundle\Entity\Fichefrais;
use GsbBundle\Entity\Lignefraisforfait;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class GererFraisController extends Controller
{
    public function accueilVisiteurAction(){
      $session = new Session();
      if($session->get('idVisiteur') == null){
        return $this->redirectToRoute('connexion');
      }
      return $this->render('@Gsb/GererFrais/accueilVisiteur.html.twig');
    }

    public function saisirFraisForfaitAction(Request $request)
    {
        $session = new Session();
        $idVisiteur = $session->get('idVisiteur');
        $fonctionService = $this->get('gsb.fonction');
        $date = new DateTime('now');
        $dateFormate = $date->format('d/m/Y');
        $mois = $fonctionService->getMois($dateFormate);
        $em = $this->getDoctrine()->getManager();
        $fichefrais = $em->getRepository('GsbBundle:Fichefrais')->estPremierFraisMois($idVisiteur, $mois);
        //dump($fichefrais);
        $leVisiteur = $em->getRepository('GsbBundle:Visiteur')->findOneByid($idVisiteur);
        //dump($leVisiteur);
        $etat = $em->getRepository('GsbBundle:Etat')->findOneBy(array('id'=>'CR'));
        //dump($etat);

        if($fichefrais == null){
          $fichefrais = new Fichefrais();
          $fichefrais->setIdvisiteur($leVisiteur)
                     ->setMois($mois)
                     ->setDatemodif($date)
                     ->setIdetat($etat);

          $em->persist($fichefrais);
          $em->flush();
          //dump($fichefrais);

          $lesIdFrais = $this->getDoctrine()->getManager()->getRepository('GsbBundle:Fraisforfait')->getLesIdFraisForfait();
          //dump($lesIdFrais);

          foreach ($lesIdFrais as $laLigneIdFrais) {
            $lignefraisforfait = new Lignefraisforfait();
            $lignefraisforfait->setIdfichefrais($fichefrais)
                              ->setIdfraisforfait($laLigneIdFrais)
                              ->setQuantite(0)
                              ->setDatemodification($date)
                              ->setIdetat($fichefrais->getIdetat());

           $em->persist($lignefraisforfait);
          }
          $em->flush();
          $lesLigneFraisForfait = $em->getRepository('GsbBundle:Fichefrais')->getLigneFraisForfait($fichefrais->getId());
          //dump($lesLigneFraisForfait);
        }
        else
        {
          $lesLigneFraisForfait = $em->getRepository('GsbBundle:Fichefrais')->getLigneFraisForfait($fichefrais->getId());
          if($lesLigneFraisForfait == null)
          {
            $lesIdFrais = $this->getDoctrine()->getManager()->getRepository('GsbBundle:Fraisforfait')->getLesIdFraisForfait();
            //dump($lesIdFrais);

            foreach ($lesIdFrais as $laLigneIdFrais) {
              $lignefraisforfait = new Lignefraisforfait();
              $lignefraisforfait->setIdfichefrais($fichefrais)
                                ->setIdfraisforfait($laLigneIdFrais)
                                ->setQuantite(0)
                                ->setDatemodification($date)
                                ->setIdetat($fichefrais->getIdetat());

             $em->persist($lignefraisforfait);
            }
            $em->flush();
          }
        }
        $lesLigneFraisForfait = $em->getRepository('GsbBundle:Fichefrais')->getLigneFraisForfait($fichefrais->getId());
        //dump($lesLigneFraisForfait);
        $form = $this->createFormBuilder($lesLigneFraisForfait)
                     ->add('label1', TextType::class, array('label'=>$lesLigneFraisForfait[0]->getIdfraisforfait()->getLibelle(), 'data'=> $lesLigneFraisForfait[0]->getQuantite()))
                     ->add('label2', TextType::class, array('label'=>$lesLigneFraisForfait[1]->getIdfraisforfait()->getLibelle(),'data'=> $lesLigneFraisForfait[1]->getQuantite()))
                     ->add('label3', TextType::class, array('label'=>$lesLigneFraisForfait[2]->getIdfraisforfait()->getLibelle(),'data'=> $lesLigneFraisForfait[2]->getQuantite()))
                     ->add('label4', TextType::class, array('label'=>$lesLigneFraisForfait[3]->getIdfraisforfait()->getLibelle(),'data'=> $lesLigneFraisForfait[3]->getQuantite()))
                     ->add('modifier', SubmitType::class, array('label'=>'Modifier'))
                     ->getForm();
        //dump($form);

        $form->handleRequest($request);

        if($form->isSubmitted())
        {
          $quantites = array();
          array_push($quantites,$form->get('label1')->getData(),$form->get('label2')->getData(),$form->get('label3')->getData(), $form->get('label4')->getData());

          for($i = 0; $i < count($lesLigneFraisForfait); $i++){
              $lesLigneFraisForfait[$i]->setQuantite(intval($quantites[$i]));
              $em->persist($lesLigneFraisForfait[$i]);
          }
          $em->flush();
          return $this->redirectToRoute('gsb_saisir_frais_forfait');
        }

      return $this->render('@Gsb/GererFrais/saisir_frais_forfait.html.twig', array('form'=>$form->createView()));
    }

    public function saisirFraisHorsForfaitAction(Request $request)
    {
      $session = new Session();
      $idVisiteur = $session->get('idVisiteur');
      $fonctionService = $this->get('gsb.fonction');
      $date = new DateTime('now');
      $dateFormate = $date->format('d/m/Y');
      $mois = $fonctionService->getMois($dateFormate);
      $em = $this->getDoctrine()->getManager();
      $laFiche = $em->getRepository('GsbBundle:Fichefrais')->estPremierFraisMois($idVisiteur, $mois);
      //dump($laFiche);
      $leVisiteur = $em->getRepository('GsbBundle:Visiteur')->findOneByid($idVisiteur);
      //dump($leVisiteur);
      $etat = $em->getRepository('GsbBundle:Etat')->findOneBy(array('id'=>'CR'));
      //dump($etat);

      if($laFiche == null){
        $laFiche = new Fichefrais();
        $laFiche->setIdvisiteur($leVisiteur)
                   ->setMois($mois)
                   ->setDatemodif($date)
                   ->setIdetat($etat);

        $em->persist($laFiche);
        $em->flush();
        //dump($laFiche);
      }

      $lesLigneFraisHorsForfait = $em->getRepository('GsbBundle:Lignefraishorsforfait')->findByidfichefrais($laFiche->getId());
      //dump($lesLigneFraisHorsForfait);
      $lignefraishorsforfait = new Lignefraishorsforfait();
      $form = $this->createForm(LignefraishorsforfaitType::class, $lignefraishorsforfait);
      $form->handleRequest($request);
      if($form->isSubmitted()){
        //dump($form->get('date')->getData());
        $lignefraishorsforfait->setIdfichefrais($laFiche)
                              ->setLibelle($form->get('libelle')->getData())
                              ->setDate($form->get('date')->getData())
                              ->setMontant($form->get('montant')->getData())
                              ->setDatemodif($date)
                              ->setIdetat($etat);

        $em->persist($lignefraishorsforfait);
        $em->flush();

        return $this->redirectToRoute('gsb_saisir_frais_hors_forfait');
      }
      //dump($lesLigneFraisHorsForfait);
      return $this->render('@Gsb/GererFrais/saisir_frais_hors_forfait.html.twig', array('form'=>$form->createView(), 'leslignesfraishorsforfait'=> $lesLigneFraisHorsForfait));
    }

    public function supprimerFraisHorsForfaitAction($id)
    {
      $em = $this->getDoctrine()->getManager();
      $laLigneFraisHorsForfait = $em->getRepository('GsbBundle:Lignefraishorsforfait')->findOneByid($id);
      $em->remove($laLigneFraisHorsForfait);
      $em->flush();

      return $this->redirectToRoute('gsb_saisir_frais_hors_forfait');
    }


}

<?php

namespace GsbBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SuivieFraisController extends Controller
{
    public function suiviePaiementAction()
    {
        return $this->render('GsbBundle:SuivieFrais:suivie_paiement.html.twig', array(
            // ...
        ));
    }

}

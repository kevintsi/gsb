<?php

namespace GsbBundle\Controller;

use GsbBundle\Entity\Fichefrais;

use GsbBundle\Form\FichefraisType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Session\Session;

class EtatFraisController extends Controller
{
    public function listeMoisAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
      $session = new Session();
      $idVisiteur = $session->get('idVisiteur');

      $fiche = new Fichefrais();

      $formFicheFrais = $this->createForm(FichefraisType::class, $fiche);

      $formFicheFrais->handleRequest($request);
      if ($formFicheFrais->isSubmitted()){
        $data = $formFicheFrais ->getData();
        $idFicheFrais = $data->getMois()->getId();

        $repoLFF = $em->getRepository('GsbBundle:Lignefraisforfait');
        $listLFF =  $repoLFF->findBy(array('idfichefrais' => $idFicheFrais));


        $repoLFHF = $em->getRepository('GsbBundle:Lignefraishorsforfait');
        $listLFHF = $repoLFHF->findBy(array('idfichefrais' => $idFicheFrais));
        return $this->render('@Gsb/EtatFrais/consulter_frais.html.twig', array('listLFF' => $listLFF, 'listLFHF' => $listLFHF));
      }

      return $this->render('@Gsb/EtatFrais/index.html.twig', array('form' => $formFicheFrais->createView()));
    }


}

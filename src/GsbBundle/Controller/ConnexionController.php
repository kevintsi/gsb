<?php

namespace GsbBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;


class ConnexionController extends Controller
{
    public function connexionAction(Request $request)
    {
      $form = $this->createFormBuilder()
                   ->add('login', TextType::class)
                   ->add('motDePasse', PasswordType::class)
                   ->add('valider', SubmitType::class)
                   ->getForm();
      $form->handleRequest($request);

      if($form->isSubmitted()){
        $data = $form->getData();
        $login = $data['login'];
        $mdp = $data['motDePasse'];
        $visiteurRepo = $this->getDoctrine()->getManager()->getRepository('GsbBundle:Visiteur');
        $donnees = $visiteurRepo->getInfoVisiteur($login, $mdp);
        if($donnees != null){
          $uneSession = new Session();
          $uneSession->set('nom', $donnees->getNom());
          $uneSession->set('prenom', $donnees->getPrenom());
          $uneSession->set('idVisiteur', $donnees->getId());
          $uneSession->set('estComptable', $donnees->getEstcomptable());

          return $this->redirectToRoute('gsb_accueil_visiteur');
        }
      }

       return $this->render('@Gsb/Connexion/connexion.html.twig', array('form'=>$form->createView()));
    }
    public function deconnexionAction(){
       $session = new Session();
       $session->invalidate();

       return $this->redirectToRoute('connexion');
    }
}

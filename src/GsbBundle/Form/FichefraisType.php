<?php

namespace GsbBundle\Form;

use Doctrine\ORM\EntityRepository;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\OptionsResolver\OptionsResolver;

class FichefraisType extends AbstractType
{
    public function getIdVisiteur()
    {
      $session = new Session();

      return $session->get('idVisiteur');
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('mois', EntityType::class, array(
          'label' => "Liste mois",
          'class' => 'GsbBundle:Fichefrais',
          'query_builder' => function (EntityRepository $er) {
            return $er->createQueryBuilder('f')
                ->where('f.idvisiteur = :idvisiteur')
                ->setParameter('idvisiteur', $this->getIdVisiteur());},
          'choice_label' => 'mois',
          'multiple' => false
        ))
        ->add('Valider', SubmitType::class);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GsbBundle\Entity\Fichefrais'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gsbbundle_fichefrais';
    }


}

<?php

namespace GsbBundle\Repository;

/**
 * LigneFraisForfaitRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LigneFraisForfaitRepository extends \Doctrine\ORM\EntityRepository
{
  public function getLesLignesFraisForfait($idFichefrais)
  {
    $queryBuilder = $this->createQueryBuilder('lff');

    $queryBuilder->join('lff.idfichefrais','ff')
                 ->andWhere('ff.id = :idfichefrais')
                 ->setParameter('idfichefrais',$idFichefrais);

    return $queryBuilder->getQuery()->getResult();
  }
}
